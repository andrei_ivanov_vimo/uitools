module.exports = function(grunt) {

  grunt.registerTask('watch', [ 'watch' ]);

  grunt.initConfig({
    notify_hooks: {
        options: {
          enabled: true,
          max_jshint_notifications: 5, // maximum number of notifications from jshint output
          title: "STATE" // defaults to the name in package.json, or will use project directory's name
        }
      },
    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: 'img/',
                src: ['**/*.{png,jpg,gif}'],
                dest:'img/'
            },{
                expand: true,
                cwd: 'images/',
                src: ['**/*.{png,jpg,gif}'],
                dest:'images/'
            }],
        }
    },
    less: {
      style: {
        files: {
          "css/bootstrap_PHIX.css": "less/bootstrap_PHIX.less",
          "css/phix-home-addition.css": "less/phix-home-addition.less",
          "css/phix-home.css": "less/phix-home.less",
          "css/phix-responsive.css": "less/phix-responsive.less"
        }
      }
    },
    cssmin: {
      combine: {
        files: {
          'css/bootstrap_NM.css': ['css/bootstrap_NM.css']
        }
      }
    },
    watch: {
      css: {
        files: ['less/*.less'],
        tasks: ['less:style'],
        options: {
          livereload: true,
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.task.run('notify_hooks');
};
